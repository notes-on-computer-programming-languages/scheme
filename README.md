# scheme

Minimalist LISP. https://en.wikipedia.org/wiki/Scheme_(programming_language)

[[_TOC_]]

![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=rustc+ocaml-base-nox+ghc+sbcl+julia+ecl+scala+racket+elixir+clojure+chezscheme+smlnj&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%Y&beenhere=1)

# Tutorials
* [r7rs tutorial](https://google.com/search?q=r7rs+tutorial)

# Which Scheme to choose?
* [*Category:Scheme*
  ](https://www.rosettacode.org/wiki/Category:Scheme)
  (Rosetta Code)
* [popular scheme implementation](https://www.google.com/search?q=popular+scheme+implementation)
---
* scheme-faq-standards: [*What Scheme implementations are there?*
  ](http://community.schemewiki.org/?scheme-faq-standards#implementations)
  (2023-09) (Community-Scheme-Wiki)
  * Implementations suitable for begginers: `Chez Scheme`, `Gambit`, `MIT Scheme` and `Racket`. (See figure below)
  * Quite beginner-friendly: `Chicken`, `Bigloo`, `Scheme48`, and `SCM`. (See figure below)
* [*Scheme Implementations*
  ](http://wiki.c2.com/?SchemeImplementations)
  (2014)
* [*an opinionated guide to scheme implementations*
  ](https://wingolog.org/archives/2013/01/07/an-opinionated-guide-to-scheme-implementations)
  2013-01

# Implementations suitable for begginers
![Implementations suitable for begginers](https://qa.debian.org/cgi-bin/popcon-png?packages=chezscheme+gambc+mit-scheme+racket&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
![Quite beginner-friendly](https://qa.debian.org/cgi-bin/popcon-png?packages=chicken-bin+scheme48+scm&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
![Others](https://qa.debian.org/cgi-bin/popcon-png?packages=gauche+stalin+scheme9+ikarus+chibi-scheme&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
Not including Guile (too popular).

# Books
* See also "How to write a scheme interpreter...: Books on Implementing Scheme" below.

---
* [*The Scheme Programming Language*](https://www.scheme.com/tspl4/),
  2009 (Fourth Edition) R. Kent Dybvig (MIT Press)
  * Chez Scheme autor
* *The Scheme Programming Language*
  2003 (3rd) R. Kent Dybvig (MIT Press)
* *The Scheme Programming Language*
  1996 (2nd) R. Kent Dybvig (Prentice-Hall)

## (fr)
* *Initiation à la programmation et aux algorithmes : Scheme*
  2020 Laurent Bloch (Éditions Technip)
* *Initiation à la programmation avec Scheme*
  2011 (2e) Laurent Bloch (Éditions Technip)
* *Programmation récursive (en Scheme) : cours et exercices corrigés*
  2004 Anne Brygoo (Dunod)

# Features of the language
* [*More Fun with Monad Do-notation in Scheme*
  ](https://el-tramo.be/blog/scheme-monads/)
  2015-07 Remko Tronçon (el-tramo.be)

# Main libraries
## Package managers and repositories
* [Akku](https://akkuscm.org/)
* [CHICKEN Eggs](http://wiki.call-cc.org/chicken-projects/egg-index-5.html)
* [Raven](https://github.com/guenchi/Raven) is a Package Manager for Chez Scheme

## General
* [slib](https://tracker.debian.org/pkg/slib)

## Numerical and algebraic
* [scmutils](https://gitlab.com/scheme-packages-demo/scmutils)
* [JACAL Symbolic Math System](http://people.csail.mit.edu/jaffer/JACAL)
  * [Ubuntu manpage](http://manpages.ubuntu.com/manpages/cosmic/man1/jacal.1.html)
  * https://github.com/barak/jacal

## Parser combinator
* [parser combinator library scheme](https://google.com/search?q=parser+combinator+library+scheme)

## Programming languages implementation
* [*Programming in Schelog*](http://ds26gte.github.io/schelog/index.html)

## Web frameworks
### Chez
* theschemer/libra a simple web framework for chez scheme

#### [Igropyr](https://guenchi.github.io/Igropyr/) based
* Ballista: Express style webframework
* [Catapult](https://guenchi.github.io/Catapult/): purely functional webframework
* [mysql](https://github.com/chclock/mysql): mySQL binding for Chez Scheme

### HTML templates
* [Liber](https://github.com/guenchi/Liber) : HTML Template

### Chicken
* [Awful](http://wiki.call-cc.org/eggref/5/awful)
* [Awful](http://wiki.call-cc.org/eggref/4/awful) (Chicken 4)

### Guile
* Artanis

# Scheme to Javascript
## Interpreters
* [BiwaScheme](https://www.biwascheme.org) (2020)
  * [*try Scheme instead of JavaScript for UI*
    ](https://dev.to/rodiongork/try-scheme-instead-of-javascript-for-ui-1836)
    2020-02 Rodion Gorkovenko
* [LIPS](https://lips.js.org/) (2020)

## Compilers
* [scheme-to-js](https://github.com/jdan/scheme-to-js) (2018)
* scm2js
  * [*Compiling Scheme to JavaScript*
    ](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.218.2399&rep=rep1&type=pdf)
    2006-04 Florian Loitsch and Manuel Serrano
* ClojureScript is not Scheme, but.
  * [*Why ClojureScript Matters*
    ](https://dev.to/kendru/why-clojurescript-matters-227f)
    2019-09 Andrew Meredith
* PureScript is not Lisp, but functional.

# Implementation standards
* [Category:Scheme (programming language) implementations](https://en.wikipedia.org/wiki/Category:Scheme_(programming_language)_implementations)

## About Standards
* [*scheme-faq-standards*](http://community.schemewiki.org/?scheme-faq-standards)
* [*R7RS versus R6RS*](https://weinholt.se/articles/r7rs-vs-r6rs/)
* [Implementation Contrasts](https://small.r7rs.org/wiki/ImplementationContrasts/)
  2016 cowan

## Scheme Requests for Implementation
* [Scheme Requests for Implementation](https://srfi.schemers.org/)

## IEEE
* Gambit (2019)
* Larceny (2017)

## R7RS large
* [*R7RS Home Page*](https://bitbucket.org/cowan/r7rs-wg1-infra/src/default/R7RSHomePage.md)
  * Chibi (2019)
  * Larceny (2017)

## R7RS small language (2013)
* [BiwaScheme](https://www.biwascheme.org) (2020)
* Chez Scheme (from Cisco 2019)
* Chibi (2019)
* Chicken (2019)
  * [r7rs](https://wiki.call-cc.org/eggref/5/r7rs) support for most of the R7RS Scheme language
  * https://tio.run/#scheme-chicken
  * [Learn X in Y minutes, where X=CHICKEN](https://learnxinyminutes.com/docs/CHICKEN/)
* Gerbil
  * [*R7RS support in Gerbil*](https://cons.io/guide/r7rs.html)
  * [on top of the Gambit runtime](https://github.com/vyzo/gerbil)
  * [Docker images](https://cons.io/guide/)
    * https://hub.docker.com/r/gerbil/scheme
* [husk-scheme](https://github.com/justinethier/husk-scheme)
  (large portion of R7RS ) (2016)
* Kawa (2017)
* MIT/GNU Scheme (mostly R7RS) (2019)
* Racket (2019)
  * [r7rs](https://pkgs.racket-lang.org/package/r7rs)
    * [racket-r7rs](https://github.com/lexi-lambda/racket-r7rs)
  * [Learn X in Y minutes, where X=racket](https://learnxinyminutes.com/docs/racket/)
  * https://tio.run/#racket

### Benchmarks and implementation completeness tests
* Look at [Scheme Benchmarks](https://ecraven.github.io/r7rs-benchmarks/)
  * See section below.

## R6RS (2007)
* Maybe less popular than R5RS or R7RS.
* [*R6RS Implementations*](http://www.r6rs.org/implementations.html)
  * [BiwaScheme](https://www.biwascheme.org)
  * Chez Scheme (from Cisco 2019)
  * Guile (2019) (most of R6RS)
  * Ikarus Scheme
  * IronScheme
  * Larceny (2017)
  * Mosh
  * PLT Racket (2019)
    * [*Standards*](https://docs.racket-lang.org/guide/standards.html)
  * Sagittarius Scheme
  * Vicare
  * Ypsilon
* [Category:R6RS Scheme](https://en.wikipedia.org/wiki/Category:R6RS_Scheme)
  * Chez Scheme (from Cisco 2019)
* [husk-scheme](https://github.com/justinethier/husk-scheme) (2016)
  * Ikarus (2008)
  * Larceny (2017)
  * Racket (2019)
    * [*Standards*](https://docs.racket-lang.org/guide/standards.html)
  * Ypsilon (2008)

## R5RS (1998)
* Chicken (2019) (mostly)
* Bigloo (2019) R5RS?
* [husk-scheme](https://hackage.haskell.org/package/husk-scheme) (2021)
* Larceny (2017)
* [LIPS](https://lips.js.org/) (2020)
* Racket (2019)
  * [*Standards*](https://docs.racket-lang.org/guide/standards.html)
* [SCM](http://people.csail.mit.edu/jaffer/SCM.html) (contains Hobbit compiler) (also published at MIT) (2015)
  * TODO check if rpm can be converted to deb with [alien](https://tracker.debian.org/pkg/alien)
* Guile (2019)
* Gambit (2019)
* Scheme48 (2014)

## R4RS
* Gambit (2019)
* Scheme48 (2014)
* JScheme (almost R4RS) (2006)
* [Stalin](https://en.wikipedia.org/wiki/Stalin_(Scheme_implementation)) 2006
* Scheme 9 from Empty Space
  * [scheme9](https://tracker.debian.org/pkg/scheme9)

# R7RS Scheme Benchmarks
* [R7RS Scheme Benchmarks](https://ecraven.github.io/r7rs-benchmarks/)
  * The number of passed tests gives an idea of compatibility.
  * For best compatibility, check test's errors.
  * Combine with Repology availability.
  * BiwaScheme is not included.
  * Best implementations are:
    * Chez [Repology](https://repology.org/project/chez-scheme/versions)
    * Racket [Repology](https://repology.org/project/racket/versions)
    * Gerbil (manual installation on top of Gambit, faster but with the same little incompatibility (2020-12)) [Repology](https://repology.org/project/gerbil-scheme/versions)
    * Gambit (fast but with a little incompatibility (2020-12)) [Repology](https://repology.org/project/gambit-c/versions)
    * Gauche (a bit slow) [Repology](https://repology.org/project/gauche/versions)
    * Sagittarius (slower than Gauche and less common) [Repology](https://repology.org/project/sagittarius-scheme/versions)
    * Chicken has littles incompatibilities 2020-12.

![R7RS debian schemes](https://qa.debian.org/cgi-bin/popcon-png?packages=racket%20chezscheme%20gauche%20chicken-bin%20gambc%20scheme-chez-srfi&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

# Distributions: Repology, Alpine, Arch, Debian
By (r7rs-benchmarks compatibility (to check)) and speed (2019-12) (see also benchmark section):
* chezscheme
* racket (takes some space on disk) (Alpine)
* gambc
* guile... (Alpine guile)
* chicken-bin (Alpine chicken)
* mit-scheme
* scheme48
* sisc
* tinyscheme
* stalin

![most installed debian schemes](https://qa.debian.org/cgi-bin/popcon-png?packages=racket%20guile-1.8%20guile-2.0%20guile-2.2%20guile-3.0%20mit-scheme&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

![middle installed debian schemes](https://qa.debian.org/cgi-bin/popcon-png?packages=chezscheme%20racket%20chicken-bin%20mit-scheme%20scheme48&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

![least installed debian schemes](https://qa.debian.org/cgi-bin/popcon-png?packages=chezscheme%20gambc%20gauche%20scheme48%20sisc%20tinyscheme%20stalin&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

# Implementation: nanopass framework
* [*Why is Idris 2 so much faster than Idris 1?*
  ](https://news.ycombinator.com/item?id=23304081)
  (2020-05) @ Hacker News
* [*Racket-on-Chez Status: February 2020*
  ](https://blog.racket-lang.org/2020/02/racket-on-chez-status.html)
  2020-02 Matthew Flatt
* [*Chez Scheme is written using the nanopass framework*
  ](https://news.ycombinator.com/item?id=15156027)
  2017-09 capnrefsmmat @ Hacker News

# Other implementations
## What about Clojure and its unofficial and experimental implementations for other platforms?
* [Clojure: Popularity](https://en.m.wikipedia.org/wiki/Clojure#Popularity) (Wikipedia)
* They may have some more functional features
  ([*Comparison of functional programming languages*
  ](https://en.m.wikipedia.org/wiki/Comparison_of_functional_programming_languages))



## In Rust
* [scheme](https://crates.io/search?q=scheme) @crates.io

# How to write a scheme interpreter...
* [*7 lines of code, 3 minutes: Implement a programming language from scratch*
  ](http://matt.might.net/articles/implementing-a-programming-language/)
* [*Learn Functional Programming by writing a Scheme in Haskell*
  ](https://0x0f0f0f.github.io/posts/2019/09/learn-functional-programming-by-writing-a-scheme-in-haskell/)

## Books on Implementing Scheme
* [*Write You A Scheme, Version 2.0*
  ](https://wespiser.com/writings/wyas/00_overview.html)
  2016-11 Adam Wespiser
* [*Write Yourself a Scheme in 48 Hours*](http://freecomputerbooks.com/Write-Yourself-a-Scheme-in-48-Hours.html)
* [Write Yourself a Scheme in 48 Hours](https://google.com/search?q=Write+Yourself+a+Scheme+in+48+Hours)
* [*Scheme 9 from Empty Space: A Guide to Implementing Scheme in C*](http://freecomputerbooks.com/Scheme-9-from-Empty-Space.html)
